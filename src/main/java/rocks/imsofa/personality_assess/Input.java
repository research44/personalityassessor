/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.personality_assess;

/**
 *
 * @author lendle
 */
public class Input {
    private double gsr1_var, gsr2_var, gsr3_var;
    private double hr1_var, hr2_var, hr3_var;

    public double getGsr1_var() {
        return gsr1_var;
    }

    public void setGsr1_var(double gsr1_var) {
        this.gsr1_var = gsr1_var;
    }

    public double getGsr2_var() {
        return gsr2_var;
    }

    public void setGsr2_var(double gsr2_var) {
        this.gsr2_var = gsr2_var;
    }

    public double getGsr3_var() {
        return gsr3_var;
    }

    public void setGsr3_var(double gsr3_var) {
        this.gsr3_var = gsr3_var;
    }

    public double getHr1_var() {
        return hr1_var;
    }

    public void setHr1_var(double hr1_var) {
        this.hr1_var = hr1_var;
    }

    public double getHr2_var() {
        return hr2_var;
    }

    public void setHr2_var(double hr2_var) {
        this.hr2_var = hr2_var;
    }

    public double getHr3_var() {
        return hr3_var;
    }

    public void setHr3_var(double hr3_var) {
        this.hr3_var = hr3_var;
    }

    @Override
    public String toString() {
        return "Input{" + "gsr1_var=" + gsr1_var + ", gsr2_var=" + gsr2_var + ", gsr3_var=" + gsr3_var + ", hr1_var=" + hr1_var + ", hr2_var=" + hr2_var + ", hr3_var=" + hr3_var + '}';
    }
    
    
}
