/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.personality_assess;

import java.util.List;

/**
 *
 * @author lendle
 */
public interface Assessor {
    public Output execute(Input input);
}
