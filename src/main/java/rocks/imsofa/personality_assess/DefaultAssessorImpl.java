/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.personality_assess;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.renjin.script.RenjinScriptEngine;

/**
 *
 * @author USER
 */
public class DefaultAssessorImpl implements Assessor {

    private Input originalInput = null, scaledInput = null;
    private Properties prop = null;

    public DefaultAssessorImpl() {
        createModelFile("Ag_class");
        createModelFile("Co_class");
        createModelFile("Ex_class");
    }
    
    private void createModelFile(String name){
        File classModel=new File(name+"_20220707.model");
        if(!classModel.exists()){
            try ( OutputStream output=new FileOutputStream(classModel);InputStream input = RDSTest.class.getClassLoader().getResource("rocks/imsofa/personality_assess/"+name+"_20220707.model").openStream()) {
                IOUtils.copy(input, output);
            } catch (IOException ex) {
                Logger.getLogger(DefaultAssessorImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public Output execute(Input input) {
        try {
            this.originalInput = input;
            this.preProcess();
            List<ResultEntry> exClass=this.assessExClass();
            List<ResultEntry> coClass=this.assessCoClass();
            List<ResultEntry> agClass=this.assessAgClass(exClass.get(0).getLevel(), coClass.get(0).getLevel());
            List<ResultEntry> esClass=this.assessEsClass(exClass.get(0).getLevel(), coClass.get(0).getLevel(), agClass.get(0).getLevel());
            List<ResultEntry> opClass=this.assessOpClass(exClass.get(0).getLevel(), coClass.get(0).getLevel(), agClass.get(0).getLevel());
            Output output=new Output();
            output.setExClassResults(exClass);
            output.setCoClassResults(coClass);
            output.setAgClassResults(agClass);
            output.setEsClassResults(esClass);
            output.setOpClassResults(opClass);
            return output;
        } catch (Exception ex) {
            Logger.getLogger(DefaultAssessorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void preProcess() {
        this.scaledInput = new Input();
        prop = new Properties();
        try ( InputStream input = RDSTest.class.getClassLoader().getResource("rocks/imsofa/personality_assess/config.properties").openStream()) {
            prop.load(input);
            double mean_gsr1_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.mean.gsr1_var"));
            double mean_gsr2_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.mean.gsr2_var"));
            double mean_gsr3_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.mean.gsr3_var"));
            double mean_hr1_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.mean.hr1_var"));
            double mean_hr2_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.mean.hr2_var"));
            double mean_hr3_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.mean.hr3_var"));

            double sd_gsr1_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.sd.gsr1_var"));
            double sd_gsr2_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.sd.gsr2_var"));
            double sd_gsr3_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.sd.gsr3_var"));
            double sd_hr1_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.sd.hr1_var"));
            double sd_hr2_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.sd.hr2_var"));
            double sd_hr3_var = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.sd.hr3_var"));

            this.scaledInput.setGsr1_var((originalInput.getGsr1_var() - mean_gsr1_var) / sd_gsr1_var);
            this.scaledInput.setGsr2_var((originalInput.getGsr2_var() - mean_gsr2_var) / sd_gsr2_var);
            this.scaledInput.setGsr3_var((originalInput.getGsr3_var() - mean_gsr3_var) / sd_gsr3_var);

            this.scaledInput.setHr1_var((originalInput.getHr1_var() - mean_hr1_var) / sd_hr1_var);
            this.scaledInput.setHr2_var((originalInput.getHr2_var() - mean_hr2_var) / sd_hr2_var);
            this.scaledInput.setHr3_var((originalInput.getHr3_var() - mean_hr3_var) / sd_hr3_var);
        } catch (IOException ex) {
            Logger.getLogger(DefaultAssessorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected List<ResultEntry> assessExClass() throws Exception {
        double gsr1_var = this.scaledInput.getGsr1_var();
        double gsr2_var = this.scaledInput.getGsr2_var();
        double hr1_var = this.scaledInput.getHr1_var();
        double hr2_var = this.scaledInput.getHr2_var();
        String delta_gsr12 = null;
        String delta_hr12 = null;

        double hThreshold_gsr12 = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.Hthreshold.delta_gsr12"));
        double lThreshold_gsr12 = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.Lthreshold.delta_gsr12"));
        double hThreshold_hr12 = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.Hthreshold.delta_hr12"));
        double lThreshold_hr12 = Double.valueOf(prop.getProperty("rocks.imsofa.personality_assess.Lthreshold.delta_hr12"));

        double delta_gsr12_value = (gsr2_var - gsr1_var) / gsr1_var;
        double delta_hr12_value = (hr2_var - hr1_var) / hr1_var;
        if (delta_gsr12_value > hThreshold_gsr12) {
            delta_gsr12 = "H";
        } else if (delta_gsr12_value < lThreshold_gsr12) {
            delta_gsr12 = "L";
        } else {
            delta_gsr12 = "M";
        }

        if (delta_hr12_value > hThreshold_hr12) {
            delta_hr12 = "H";
        } else if (delta_hr12_value < lThreshold_hr12) {
            delta_hr12 = "L";
        } else {
            delta_hr12 = "M";
        }

        RenjinScriptEngine engine = new RenjinScriptEngine();
        engine.eval("library(randomForest)");
        engine.eval("a=readRDS('Ex_class_20220707.model')");
        engine.eval("gsr2_var=c(" + gsr2_var + ")");
        engine.eval("delta_gsr12=factor(c('" + delta_gsr12 + "'), levels=c('H', 'L','M'))");
        engine.eval("delta_hr12=factor(c('" + delta_hr12 + "'), levels=c('H', 'L','M'))");
        engine.eval("d=data.frame(gsr2_var, delta_gsr12, delta_hr12)");
        engine.eval("result=predict(a, d)");
        org.renjin.sexp.IntArrayVector result = (org.renjin.sexp.IntArrayVector) engine.get("result");
        List<ResultEntry> ret = new ArrayList<>();
        if (result.getElementAsInt(0) == 1) {
            ret.addAll(ResultEntry.createHLList(0.89));
        } else {
            ret.addAll(ResultEntry.createLHList(0.89));
        }
        return ret;
    }

    protected List<ResultEntry> assessCoClass() throws Exception {
        double gsr2_var = this.scaledInput.getGsr2_var();
        double gsr3_var = this.scaledInput.getGsr3_var();
        double hr1_var = this.scaledInput.getHr1_var();
        double hr3_var = this.scaledInput.getHr3_var();

        double delta_gsr23 = (gsr3_var - gsr2_var) / gsr2_var;
        double delta_hr13 = (hr3_var - hr1_var) / hr1_var;

        RenjinScriptEngine engine = new RenjinScriptEngine();
        engine.eval("library(randomForest)");
        engine.eval("a=readRDS('Co_class_20220707.model')");
        engine.eval("gsr2_var=c(" + gsr2_var + ")");
        engine.eval("delta_gsr23=c(" + delta_gsr23 + ")");
        engine.eval("delta_hr13=c(" + delta_hr13 + ")");
        engine.eval("d=data.frame(gsr2_var, delta_gsr23, delta_hr13)");
        engine.eval("result=predict(a, d)");
        org.renjin.sexp.IntArrayVector result = (org.renjin.sexp.IntArrayVector) engine.get("result");
        List<ResultEntry> ret = new ArrayList<>();
        if (result.getElementAsInt(0) == 1) {
            ret.addAll(ResultEntry.createHLList(0.76));
        } else {
            ret.addAll(ResultEntry.createLHList(0.76));
        }
        return ret;
    }

    protected List<ResultEntry> assessAgClass(rocks.imsofa.personality_assess.Level exClass, rocks.imsofa.personality_assess.Level coClass) throws Exception {
        double hr2_var = this.scaledInput.getHr2_var();

        RenjinScriptEngine engine = new RenjinScriptEngine();
        engine.eval("library(randomForest)");
        engine.eval("a=readRDS('Ag_class_20220707.model')");
        engine.eval("Ex_class=factor(c('" + ((exClass.equals(rocks.imsofa.personality_assess.Level.HIGH) ? "H" : "L")) + "'), levels=c('H', 'L'))");
        engine.eval("Co_class=factor(c('" + ((coClass.equals(rocks.imsofa.personality_assess.Level.HIGH) ? "H" : "L")) + "'), levels=c('H', 'L'))");
        engine.eval("hr2_var=c(" + hr2_var + ")");
        engine.eval("d=data.frame(Ex_class, Co_class, hr2_var)");
        engine.eval("result=predict(a, d)");
        org.renjin.sexp.IntArrayVector result = (org.renjin.sexp.IntArrayVector) engine.get("result");
        List<ResultEntry> ret = new ArrayList<>();
        if (result.getElementAsInt(0) == 1) {
            ret.addAll(ResultEntry.createHLList(0.72));
        } else {
            ret.addAll(ResultEntry.createLHList(0.72));
        }
        return ret;
    }

    protected List<ResultEntry> assessEsClass(
            rocks.imsofa.personality_assess.Level exClass,
            rocks.imsofa.personality_assess.Level coClass,
            rocks.imsofa.personality_assess.Level agClass) {
        List<ResultEntry> ret = new ArrayList<>();
        if (exClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && coClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && agClass.equals(rocks.imsofa.personality_assess.Level.LOW)) {
            ret.addAll(ResultEntry.createHLList(0.5));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && coClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && agClass.equals(rocks.imsofa.personality_assess.Level.LOW)) {
            ret.addAll(ResultEntry.createLHList(0.75));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && coClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && agClass.equals(rocks.imsofa.personality_assess.Level.HIGH)) {
            ret.addAll(ResultEntry.createLHList(0.67));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && coClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && agClass.equals(rocks.imsofa.personality_assess.Level.LOW)) {
            ret.addAll(ResultEntry.createLHList(0.75));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && coClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && agClass.equals(rocks.imsofa.personality_assess.Level.HIGH)) {
            ret.addAll(ResultEntry.createHLList(0.9));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && coClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && agClass.equals(rocks.imsofa.personality_assess.Level.HIGH)) {
            ret.addAll(ResultEntry.createLHList(0.71));
        }else{
            ret.addAll(ResultEntry.createHLList(0.5));
        }
        return ret;
    }
    
    protected List<ResultEntry> assessOpClass(
            rocks.imsofa.personality_assess.Level exClass,
            rocks.imsofa.personality_assess.Level coClass,
            rocks.imsofa.personality_assess.Level agClass) {
        List<ResultEntry> ret = new ArrayList<>();
        if (exClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && coClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && agClass.equals(rocks.imsofa.personality_assess.Level.LOW)) {
            ret.addAll(ResultEntry.createLHList(0.75));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && coClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && agClass.equals(rocks.imsofa.personality_assess.Level.LOW)) {
            ret.addAll(ResultEntry.createHLList(0.75));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && coClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && agClass.equals(rocks.imsofa.personality_assess.Level.HIGH)) {
            ret.addAll(ResultEntry.createHLList(0.67));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && coClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && agClass.equals(rocks.imsofa.personality_assess.Level.LOW)) {
            ret.addAll(ResultEntry.createLHList(0.75));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && coClass.equals(rocks.imsofa.personality_assess.Level.LOW)
                && agClass.equals(rocks.imsofa.personality_assess.Level.HIGH)) {
            ret.addAll(ResultEntry.createHLList(0.9));
        }else if (exClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && coClass.equals(rocks.imsofa.personality_assess.Level.HIGH)
                && agClass.equals(rocks.imsofa.personality_assess.Level.HIGH)) {
            ret.addAll(ResultEntry.createHLList(0.57));
        }else{
            ret.addAll(ResultEntry.createHLList(0.5));
        }
        return ret;
    }

    public static void main(String[] args) throws Exception {
        Input input = new Input();
        input.setGsr1_var(100);
        input.setGsr2_var(150);
        input.setGsr3_var(100);
        input.setHr1_var(50);
        input.setHr2_var(30);
        input.setHr3_var(50);
        AssessorFactory assessorFactory = new AssessorFactory();
        Assessor assessor = assessorFactory.newAssessor();
        Output output=assessor.execute(input);
        System.out.println(output.getExClassResults().get(0).getLevel());
        System.out.println(output.getCoClassResults().get(0).getLevel());
        System.out.println(output.getAgClassResults().get(0).getLevel());
        System.out.println(output.getEsClassResults().get(0).getLevel());
        System.out.println(output.getOpClassResults().get(0).getLevel());
    }
}
