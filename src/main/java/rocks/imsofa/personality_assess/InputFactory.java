/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.personality_assess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author lendle
 */
public class InputFactory {

    private final static long MIN_INTERVAL = 7 * 60 * 1000;
    public static Input createInput(List<RawSignal> gsrValues, List<RawSignal> hrValues) throws Exception {
        return createInput(gsrValues, hrValues, false);
    }
    public static Input createInput(List<RawSignal> gsrValues, List<RawSignal> hrValues, boolean allowInsufficientData) throws Exception {
        gsrValues = sortByTimestamp(gsrValues);
        hrValues = sortByTimestamp(hrValues);
        if (!allowInsufficientData && (((gsrValues.get(gsrValues.size() - 1).getTimestamp() - gsrValues.get(0).getTimestamp()) < MIN_INTERVAL)
                || (hrValues.get(hrValues.size() - 1).getTimestamp() - hrValues.get(0).getTimestamp()) < MIN_INTERVAL)) {
            throw new Exception("must be at least 7 minutes");
        }
        List<Double>[] segmentedGsr = segmentValues(gsrValues);
        List<Double>[] segmentedHr = segmentValues(hrValues);
        Input input = new Input();
        input.setGsr1_var(new DescriptiveStatistics(convert(segmentedGsr[0].toArray(new Double[0]))).getVariance());
        input.setGsr2_var(new DescriptiveStatistics(convert(segmentedGsr[1].toArray(new Double[0]))).getVariance());
        input.setGsr3_var(new DescriptiveStatistics(convert(segmentedGsr[2].toArray(new Double[0]))).getVariance());
        input.setHr1_var(new DescriptiveStatistics(convert(segmentedHr[0].toArray(new Double[0]))).getVariance());
        input.setHr2_var(new DescriptiveStatistics(convert(segmentedHr[1].toArray(new Double[0]))).getVariance());
        input.setHr3_var(new DescriptiveStatistics(convert(segmentedHr[2].toArray(new Double[0]))).getVariance());
        return input;
    }

    private static double[] convert(Double[] origin) {
        double[] ret = new double[origin.length];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = origin[i];
        }
        return ret;
    }

    //divide data into 3 segments
    private static List<Double>[] segmentValues(List<RawSignal> values) {
        List<Double> ret[] = new List[]{new ArrayList<>(), new ArrayList<>(), new ArrayList<>()};
        long startTime = values.get(0).getTimestamp();
        long totalInterval=getTotalInterval(values);
        double firstEnd = ((totalInterval<MIN_INTERVAL)?totalInterval:MIN_INTERVAL) / 3.0;
        for (RawSignal signal : values) {
            if ((signal.getTimestamp() - startTime) < firstEnd) {
                ret[0].add(signal.getValue());
            } else if ((signal.getTimestamp() - startTime) < firstEnd * 2) {
                ret[1].add(signal.getValue());
            } else if ((signal.getTimestamp() - startTime) < firstEnd * 3) {
                ret[2].add(signal.getValue());
            }
        }
        return ret;
    }
    
    private static long getTotalInterval(List<RawSignal> values) {
        return values.get(values.size() - 1).getTimestamp() - values.get(0).getTimestamp();
    }

    private static List<RawSignal> sortByTimestamp(List<RawSignal> values) {
        List<RawSignal> copy = new ArrayList<>(values);
        Collections.sort(copy, new Comparator<RawSignal>() {
            @Override
            public int compare(RawSignal o1, RawSignal o2) {
                return (int) (o1.getTimestamp() - o2.getTimestamp());
            }
        });
        return copy;
    }

    public static void main(String[] args) throws Exception {
        List<RawSignal> gsrValues = new ArrayList<>();
        List<RawSignal> hrValues = new ArrayList<>();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 8 * 60 * 1000; i+=200) {
            long timestamp = start + i;
            double gsr = 230+50 * Math.random();
            double hr = 50+90 * Math.random();
            gsrValues.add(new RawSignal(timestamp, gsr));
            hrValues.add(new RawSignal(timestamp, hr));
        }
        System.out.println(Arrays.deepToString(gsrValues.toArray()));
        Input input = InputFactory.createInput(gsrValues, hrValues);
        System.out.println(input);
        AssessorFactory assessorFactory = new AssessorFactory();
        Assessor assessor = assessorFactory.newAssessor();
        Output output = assessor.execute(input);
        System.out.println(output.getExClassResults().get(0).getLevel());
        System.out.println(output.getCoClassResults().get(0).getLevel());
        System.out.println(output.getAgClassResults().get(0).getLevel());
        System.out.println(output.getEsClassResults().get(0).getLevel());
        System.out.println(output.getOpClassResults().get(0).getLevel());
    }
}
