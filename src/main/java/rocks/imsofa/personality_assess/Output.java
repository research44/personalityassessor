/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.personality_assess;

import java.util.List;

/**
 *
 * @author USER
 */
public class Output {
    private List<ResultEntry> exClassResults, coClassResults, agClassResults, esClassResults, opClassResults;

    public List<ResultEntry> getExClassResults() {
        return exClassResults;
    }

    public void setExClassResults(List<ResultEntry> exClassResults) {
        this.exClassResults = exClassResults;
    }

    public List<ResultEntry> getCoClassResults() {
        return coClassResults;
    }

    public void setCoClassResults(List<ResultEntry> coClassResults) {
        this.coClassResults = coClassResults;
    }

    public List<ResultEntry> getAgClassResults() {
        return agClassResults;
    }

    public void setAgClassResults(List<ResultEntry> agClassResults) {
        this.agClassResults = agClassResults;
    }

    public List<ResultEntry> getEsClassResults() {
        return esClassResults;
    }

    public void setEsClassResults(List<ResultEntry> esClassResults) {
        this.esClassResults = esClassResults;
    }

    public List<ResultEntry> getOpClassResults() {
        return opClassResults;
    }

    public void setOpClassResults(List<ResultEntry> opClassResults) {
        this.opClassResults = opClassResults;
    }
    
}
