/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package rocks.imsofa.personality_assess;

import java.io.InputStream;
import java.util.Properties;
import org.renjin.script.RenjinScriptEngine;

/**
 *
 * @author lendle
 */
public class RDSTest {

    public static void main(String[] args) throws Exception {
        {
            RenjinScriptEngine engine = new RenjinScriptEngine();
            engine.eval("library(randomForest)");
            engine.eval("a=readRDS('Ex_class.model')");
            engine.eval("print(a)");
            engine.eval("gsr2_var=c(1.2, -0.5, -1, 5)");
            engine.eval("delta_gsr12=factor(c('L', 'M', 'M', 'M'), levels=c('H', 'L','M'))");
            engine.eval("delta_hr12=factor(c('M','M', 'M', 'M'), levels=c('H', 'L','M'))");
            engine.eval("d=data.frame(gsr2_var, delta_gsr12, delta_hr12)");
            engine.eval("str(d)");
            //engine.eval("d$delta_gsr12=factor(d$delta_gsr12)");
            //engine.eval("d$delta_hr12=factor(d$delta_hr12)");
            engine.eval("print(d)");
            engine.eval("result=predict(a, d)");
            engine.eval("print(result)");
            org.renjin.sexp.IntArrayVector result = (org.renjin.sexp.IntArrayVector) engine.get("result");
            System.out.println(result.getElementAsInt(0));
        }
        
        {
            RenjinScriptEngine engine = new RenjinScriptEngine();
            engine.eval("library(randomForest)");
            engine.eval("a=readRDS('Co_class.model')");
            engine.eval("print(a)");
            engine.eval("gsr2_var=c(1.2, -0.5, -1, 5)");
            engine.eval("delta_gsr23=c(-1, -0.5, 0.5, 1)");
            engine.eval("delta_hr13=c(-10, -5, 5, 10)");
            engine.eval("d=data.frame(gsr2_var, delta_gsr23, delta_hr13)");
            engine.eval("str(d)");
            //engine.eval("d$delta_gsr12=factor(d$delta_gsr12)");
            //engine.eval("d$delta_hr12=factor(d$delta_hr12)");
            engine.eval("print(d)");
            engine.eval("result=predict(a, d)");
            engine.eval("print(result)");
            org.renjin.sexp.IntArrayVector result = (org.renjin.sexp.IntArrayVector) engine.get("result");
            System.out.println(result.getElementAsInt(0));
        }
        
        {
            RenjinScriptEngine engine = new RenjinScriptEngine();
            engine.eval("library(randomForest)");
            engine.eval("a=readRDS('Ag_class.model')");
            engine.eval("print(a)");
            engine.eval("Ex_class=factor(c('H','H','L','L'), levels=c('H','L'))");
            engine.eval("Co_class=factor(c('L','H','L','H'), levels=c('H','L'))");
            engine.eval("hr2_var=c(0,0.5,1,1.5)");
            engine.eval("d=data.frame(Ex_class, Co_class, hr2_var)");
            engine.eval("str(d)");
            //engine.eval("d$delta_gsr12=factor(d$delta_gsr12)");
            //engine.eval("d$delta_hr12=factor(d$delta_hr12)");
            engine.eval("print(d)");
            engine.eval("result=predict(a, d)");
            engine.eval("print(result)");
            org.renjin.sexp.IntArrayVector result = (org.renjin.sexp.IntArrayVector) engine.get("result");
            System.out.println(result.getElementAsInt(0));
        }
        
        Properties prop = new Properties();
        try(InputStream input=RDSTest.class.getClassLoader().getResource("rocks/imsofa/personality_assess/config.properties").openStream()){
            prop.load(input);
            System.out.print(prop.getProperty("rocks.imsofa.personality_assess.mean.gsr1_var"));
        }
        
    }
}
