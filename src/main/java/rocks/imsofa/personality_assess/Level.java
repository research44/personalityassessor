/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package rocks.imsofa.personality_assess;

/**
 *
 * @author lendle
 */
public enum Level {
    HIGH,
    LOW
}
