/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.personality_assess;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class ResultEntry {

    private Level level = Level.HIGH;
    private double confidence = 0.5;

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public static List<ResultEntry> createHLList(double hConfidence) {
        List<ResultEntry> ret = new ArrayList<>();
        ResultEntry entry1 = new ResultEntry();
        entry1.setLevel(rocks.imsofa.personality_assess.Level.HIGH);
        entry1.setConfidence(hConfidence);
        ret.add(entry1);

        ResultEntry entry2 = new ResultEntry();
        entry2.setLevel(rocks.imsofa.personality_assess.Level.LOW);
        entry2.setConfidence(1-hConfidence);
        ret.add(entry2);
        return ret;
    }
    
    public static List<ResultEntry> createLHList(double lConfidence) {
        List<ResultEntry> ret = new ArrayList<>();
        ResultEntry entry1 = new ResultEntry();
        entry1.setLevel(rocks.imsofa.personality_assess.Level.LOW);
        entry1.setConfidence(lConfidence);
        ret.add(entry1);

        ResultEntry entry2 = new ResultEntry();
        entry2.setLevel(rocks.imsofa.personality_assess.Level.HIGH);
        entry2.setConfidence(1-lConfidence);
        ret.add(entry2);
        return ret;
    }
}
